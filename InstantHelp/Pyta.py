import mysql.connector
import kivy

from kivy.app import App
from kivy.uix.image import Image
from kivy.uix.widget import Widget
from kivy.uix.button import Button
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.textinput import TextInput
from kivy.graphics import BorderImage
from kivy.uix.label import Label
from kivy.uix.screenmanager import ScreenManager
from kivy.uix.screenmanager import Screen
from kivy.uix.screenmanager import RiseInTransition
from kivy.uix.scrollview import ScrollView
from kivy.core.window import Window
from kivy.uix.popup import Popup
from kivy.lang import Builder
from kivy.uix.dropdown import DropDown
from kivy.uix.boxlayout import BoxLayout
Builder.load_file("Pyta.kv")


cnx=mysql.connector.connect(user='root',password='',host='localhost',database='InstantHelp')
cursor = cnx.cursor()

class TelaLogin(Screen):
    def pegad(self):
        user = self.ids.usuario.text
        passw = self.ids.senha.text
        query = ("select NomeUsuario from usuario where NomeUsuario= %s and SenhaUsuario= %s")
        cursor.execute(query, (user, passw,))
        nme = cursor.fetchone()
        print(nme)
        if nme == None:
            popup = Popup(title='Erro!', content=Label(text='Usuario ou senha incoretos!'), size_hint=(None, None),
                          size=(400, 200))
            popup.open()
        else:
            self.ids.teste.text = 'USUÁRIO CORRETO! Conectando..'
            self.manager.current = 'Tmenu'

class TelaCadas(Screen):

    def idadebotao(self):
        IdadeDropDown= DropDown()

        for index in range(10):
            btn= Button(text='Value %d' % index, size_hint_y=None, height=44)
            btn.bind(on_release=lambda btn: dropdown.select(btn.text))
            IdadeDropDown.add_widget(btn)

        mainbuttonI= Button(size_hint=(None,None), text='Hello')
        mainbuttonI.bind(on_release=IdadeDropDown.open)
        IdadeDropDown.bind(on_select=lambda instance, x: setattr(mainbuttonI, 'text', x))

        self.add_widget(mainbuttonI)


    def voltal(self):
        self.manager.current='Tlogin'

    def Icadas(self):
        user=self.ids.usuario
        passw=self.ids.senha
        email=self.ids.email



class MenuPrin(Screen):
    pass

class manager(ScreenManager):
    pass

manager.transition = RiseInTransition()

class Pyta(App):

    def build(self):
        self.title='Instant Help'
        self.icon='icob.png'
        return manager()

Pyta().run()
