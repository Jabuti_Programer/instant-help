CREATE TABLE categoria (
  idcategoria INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  NomeCategoria CHAR(50) NOT NULL,
  PRIMARY KEY(idcategoria)
)
TYPE=InnoDB;

CREATE TABLE cidade (
  idcidade INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  estado_idestado INTEGER UNSIGNED NOT NULL,
  NomeCidade CHAR(40) NOT NULL,
  PRIMARY KEY(idcidade),
  INDEX cidade_FKIndex1(estado_idestado)
)
TYPE=InnoDB;

CREATE TABLE estado (
  idestado INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  pa�s_idpa�s INTEGER UNSIGNED NOT NULL,
  NomeEstado CHAR(50) NOT NULL,
  PRIMARY KEY(idestado),
  INDEX estado_FKIndex1(pa�s_idpa�s)
)
TYPE=InnoDB;

CREATE TABLE pa�s (
  idpa�s INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  nome CHAR(15) NOT NULL,
  PRIMARY KEY(idpa�s)
)
TYPE=InnoDB;

CREATE TABLE Pedido (
  idPedido INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  usuario_idusuario INTEGER UNSIGNED ZEROFILL NOT NULL,
  problemas_idproblemas INTEGER UNSIGNED NOT NULL,
  Situa��o descritiva VARCHAR(255) NULL,
  PRIMARY KEY(idPedido),
  INDEX Pedido_FKIndex1(problemas_idproblemas),
  INDEX Pedido_FKIndex2(usuario_idusuario)
)
TYPE=InnoDB;

CREATE TABLE problemas (
  idproblemas INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  categoria_idcategoria INTEGER UNSIGNED NOT NULL,
  urg�ncia_IDurg�ncia INTEGER UNSIGNED NOT NULL,
  Nome CHAR(50) NOT NULL,
  PRIMARY KEY(idproblemas),
  INDEX problemas_FKIndex1(urg�ncia_IDurg�ncia),
  INDEX problemas_FKIndex2(categoria_idcategoria)
)
TYPE=InnoDB;

CREATE TABLE Reputa��o (
  idReputa��o INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  idNome CHAR(50) NOT NULL,
  PRIMARY KEY(idReputa��o)
)
TYPE=InnoDB;

CREATE TABLE urg�ncia (
  IDurg�ncia INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  nome CHAR(50) NOT NULL,
  PRIMARY KEY(IDurg�ncia)
)
TYPE=InnoDB;

CREATE TABLE usuario (
  idusuario INTEGER UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT,
  Reputa��o_idReputa��o INTEGER UNSIGNED NOT NULL,
  cidade_idcidade INTEGER UNSIGNED NOT NULL,
  NomeUsuario CHAR(50) NOT NULL,
  IdadeUsuario INTEGER(2) UNSIGNED NOT NULL,
  SenhaUsuario CHAR(15) NOT NULL,
  SexoUsuario CHAR() NOT NULL,
  Endere�oUsuario VARCHAR(255) NULL,
  PRIMARY KEY(idusuario),
  INDEX usuario_FKIndex2(cidade_idcidade),
  INDEX usuario_FKIndex2(Reputa��o_idReputa��o)
)
TYPE=InnoDB;


